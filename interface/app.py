from flask import Flask, render_template, url_for
app = Flask(__name__)

import os;


@app.route("/")
@app.route("/home")
def home():
    return render_template('home.html')


    
@app.route("/about")
def about():
    return render_template('about.html', title='About')

@app.route('/ddosgen')
def ddosgen():
    os.system("gnome-terminal -e 'bash -c \"sudo apt-get update; exec bash\"'")
    


if __name__ == '__main__':
    app.run(debug=True)